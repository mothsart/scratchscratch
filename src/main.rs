#[macro_use]
extern crate html5ever;

use std::default::Default;
use std::iter::repeat;
use std::string::String;
use std::fmt::Write;

use html5ever::parse_document;
use html5ever::rcdom::{Handle, NodeData, RcDom};
use html5ever::tendril::TendrilSink;

struct ShortElement {
	indent: usize,
	value: String
}

impl PartialEq for ShortElement {
	fn eq(&self, other: &Self) -> bool {
        self.indent == other.indent && self.value == other.value
    }
}

fn follow_br(
	first_element: &ShortElement,
	second_element: &ShortElement
) {
	if  first_element.value == "br" && first_element == second_element {
		println!("2 <br> qui se suivent!");
	}
}

fn img_alt_empty(
	element_name: String,
	attr_value: String
) {
	if element_name == "img" && attr_value == "" {
		println!("Balise <img> : attribut alt vide");
	}
}

struct AttrNeeded {
	value: String,
	message: String
}

struct NeedAttrElement {
	name: String,
	attr: Vec<AttrNeeded>,
	attr_exist: Vec<AttrNeeded>
}

impl NeedAttrElement {
	fn new (name: String, attrs: Vec<AttrNeeded>) -> NeedAttrElement {
		NeedAttrElement {
			name: name,
			attr: attrs,
			attr_exist: Vec::new()
		}
	}

	fn _has_attr(
		&self,
		attr_value: String
	) -> Option<usize> {
		for (index, attr) in self.attr.iter().enumerate() {
			if attr.value == attr_value {
				return Some(index);
			}
		}
		return None;
	}

	fn has_attr(
		&mut self,
		attr_value: String
	) -> bool {
	    let _value = self._has_attr(attr_value);
		match _value {
			Some(_x) => {
				&self.attr_exist.push(self.attr.remove(_x));
				true
			},
			None => { false }
		}
	}

	fn print(&self) {
		for attr in &self.attr {
			println!("{}", attr.message);
		}
	}
}

fn serialize(
	buf: &mut String,
	inc: &mut i32,
	indent: usize,
	last_element: &mut ShortElement,
	handle: Handle
) {
	if *inc != 1 {
    	buf.push_str(&format!("{}", inc));
	}
    buf.push_str(&repeat(" ").take(indent).collect::<String>());

    let node = handle;
    match node.data {
        NodeData::Text { ref contents } => {
			*inc = *inc + 1;
            buf.push_str("\n");
        },

        NodeData::Element {
            ref name,
            ref attrs,
            ..
        } => {
			*inc = *inc + 1;
            buf.push_str("<");
            match name.ns {
                ns!(svg) => buf.push_str("svg "),
                ns!(mathml) => buf.push_str("math "),
                _ => (),
            }
            buf.push_str(&*name.local);
			// tests métiers
			follow_br(
				&ShortElement {
					indent: indent,
					value: name.local.to_string()
				},
			    &*last_element
			);

            let mut attrs = attrs.borrow().clone();
            attrs.sort_by(|x, y| x.name.local.cmp(&y.name.local));
            // FIXME: sort by UTF-16 code unit

            let mut _el = NeedAttrElement::new(
				"img".to_string(), 
				vec![ 
					AttrNeeded {
						value: "alt".to_string(), 
						message: "Balise <img> sans attribut \"alt\"".to_string()
					} 
				]
			);
            for attr in attrs.into_iter() {
				img_alt_empty(name.local.to_string(), attr.value.to_string());
                buf.push_str(" ");
                match attr.name.ns {
                    ns!(xlink) => buf.push_str("xlink "),
                    ns!(xml) => buf.push_str("xml "),
                    ns!(xmlns) => buf.push_str("xmlns "),
                    _ => (),
                }
				_el.has_attr(attr.name.local.to_string());
                buf.push_str(&format!("{}=\"{}\"", attr.name.local, attr.value));
            }
			if (name.local.to_string() == "img".to_string()) {
				_el.print();
			}
			buf.push_str(">\n");
			*last_element = ShortElement {
				indent: indent,
				value: name.local.to_string()
			};
        },
        NodeData::ProcessingInstruction { .. } => unreachable!(),
		_ => {}
    }

    for child in node.children.borrow().iter() {
        serialize(buf, inc, indent + 2, last_element, child.clone());
    }

    if let NodeData::Element {
        template_contents: Some(ref content),
        ..
    } = node.data
    {
        buf.push_str("|");
        buf.push_str(&repeat(" ").take(indent + 2).collect::<String>());
        buf.push_str("content\n");
        for child in content.children.borrow().iter() {
            serialize(buf, inc, indent + 4, last_element, child.clone());
        }
    }
}

// FIXME: Copy of str::escape_default from std, which is currently unstable
pub fn escape_default(s: &str) -> String {
    s.chars().flat_map(|c| c.escape_default()).collect()
}

fn parse_html(html: &str) -> String {
    let dom = parse_document(RcDom::default(), Default::default())
        .from_utf8()
        .read_from(&mut html.as_bytes())
        .unwrap();
	let mut result = String::new();
	let mut inc = 1;
	let mut last_element = ShortElement {
		indent: 0,
		value: String::new()
	};
    serialize(&mut result, &mut inc, 0, &mut last_element, dom.document);
	println!("1{}", result);

    if !dom.errors.is_empty() {
        println!("\nParse errors:");
        for err in dom.errors.iter() {
            println!("    {}", err);
        }
    }
	return result;
}

fn main() {
	let html = r#"
		<!DOCTYPE html>
		<img src="djsfhsj" alt="">
		<img src="djsfhsj" />
		<div>
			<div>cool<br></div>
			<br>
		</div>
		<br>
	"#;
	parse_html(html);
}

/*
#[cfg(test)]
mod tests {
    #[test]
    fn test_tokenize() {
		let html = r#"
			<!DOCTYPE html>
			<img src="djsfhsj" alt="" />
			<img src="djsfhsj" />
			<div>
				<div>cool<br></div>
				<br>
			</div>
			<br>
		"#;
		parse_html(html);
	}
}
*/